#include <iostream>
using namespace std;

int main()
{
  long sum = 0;
  long x, y;

  std::cout << "Enter two numbers: ";
  std::cin >> x >> y;

  if (x > y)
    {
      int temp;

      temp = x;
      x = y;
      y = temp;

    }

 	for( int counter = x; counter <= y; counter++ )
		sum += counter;

	cout << "\nSum of numbers from " << x << " to " << y
	     << " = " << sum << endl;

	return 0;
} 
