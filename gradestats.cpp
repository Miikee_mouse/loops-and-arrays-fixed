#include <iostream>

int main()
{
  const int MAX_SIZE = 100;
  int grades[MAX_SIZE];
  int qty = 0;
  int grade;

  do {
    std::cout << "Enter grade (or -1 to end): ";
    std::cin >> grade;

    if (grade != -1) {
      grades[qty] = grade;
      ++qty;
    }
    
  } while (grade != -1);

  double average = 0;
  for (int i = 0; i < qty; ++i)
    average += grades[i];
  average = average/qty;

  std::cout << "Average: " << average << '\n';
  return 0;

}
